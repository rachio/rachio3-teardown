# rachio3-teardown

![Rachio3](images/rachio3.png)

Teardown and component identification of the [Rachio3](https://rachio.com/rachio-3/) Smart WiFi Sprinkler Controller

## FCC Certification

### OET Exhibits List
[FCC Exhibits](https://apps.fcc.gov/oetcf/eas/reports/ViewExhibitReport.cfm?mode=Exhibits&RequestTimeout=500&calledFromFrame=Y&application_id=VxpPEGZXJz0mmVZgDsdzLA%3D%3D&fcc_id=2AOTB-ZULWC)

## PCA Board
![board](images/01-board.png)

![board](images/00-layout.png)

## U2 Microcontroller

[muRata Shielded Ultra Small Dual Band WiFi 11a/b/g/n+Ethernet+MCU Module](https://wireless.murata.com/type1gc.html)

### Part number

    Model:
    Type1GC
    SS8D18006

### Datasheet

[Vendor datasheet](https://wireless.murata.com/datasheet?/RFM/data/lbwa1uz1gc-958.pdf)

[Cached datasheet](datasheets/lbwa1uz1gc-958.pdf)

![mcu](images/02-mcu.png)

## U3 LoRa Transciever

[Semtech SX1272
Long Range, Low Power RF Transceiver 860-1000 MHz with LoRa Technology](https://www.semtech.com/products/wireless-rf/lora-transceivers/sx1272)

### Part number

    1272
    1836
    204668

### Datasheet

[Vendor datasheet](https://www.semtech.com/uploads/documents/SX1272_DS_V4.pdf)

[Cached datasheet](datasheets/SX1272_DS_V4.pdf)

![thyristor](images/03-lora.png)

## U4 Thyristor

[Thyristor Littelfuse, Inc. Series 1 Amp Sensitive Triacs](https://www.littelfuse.com/products/power-semiconductors/discrete-thyristors/triac/l01/l0107mt.aspx)

### Part number

    L0107MT
    9AC19

### Datasheet

[Vendor datasheet](https://www.littelfuse.com/~/media/electronics/datasheets/switching_thyristors/littelfuse_thyristor_l01_datasheet.pdf)

[Cached datasheet](datasheets/lbwa1uz1gc-958.pdf)

![thyristor](images/04-thyristor.png)

## U6 Unknown

[Unknown component]()

### Part number

    S4508
    3236A

### Datasheet

[Vendor datasheet]()

[Cached datasheet]()

![amplifier](images/06-unknown.png)

## U7 Unknown

[Unknown component]()

### Part number

    S4508
    3236A

### Datasheet

[Vendor datasheet]()

[Cached datasheet]()

![amplifier](images/07-unknown.png)

## U8 Amplifier

[Texas Instruments Quadruple general-purpose operational amplifier](http://www.ti.com/product/LM2902)

### Part number

    89A138K G4
    LM2902

### Datasheet

[Vendor datasheet](http://www.ti.com/lit/ds/symlink/lm2902.pdf)

[Cached datasheet](datasheets/lm2902.pdf)

![amplifier](images/05-amplifier.png)

## U9 Unknown QXZ

[Unknown component]()

### Part number

    L16A
    0323
    ZSD839A

### Datasheet

[Vendor datasheet]()

[Cached datasheet]()

![amplifier](images/09-unknown.png)

## U10 Real-Time Clock (RTC)

[NXP PCF2123: SPI Real-time clock/calendar](https://www.nxp.com/products/analog/signal-chain/real-time-clocks/rtcs-with-spi/spi-real-time-clock-calendar:PCF2123)

### Part number

    PCF2123
    111.1jV
    knD
    18401

### Datasheet

[Vendor datasheet](https://www.nxp.com/docs/en/data-sheet/PCF2123.pdf)

[Cached datasheet](datasheets/PCF2123.pdf)

![amplifier](images/10-rtc.png)

## U11 Integrated Silicon Solution Inc. (ISSI) Flash

[ISSI 64M-BIT 3V SERIAL FLASH MEMORY WITH MULTI I/O SPI & QUAD I/O QPI DTR INTERFACE](http://www.issi.com/US/product-flash.shtml#jump6)

### Part number

    IS25LP064
    ABLE 1747
    P1G660T4

### Datasheet

[Vendor datasheet](http://www.issi.com/WW/pdf/IS25LP032-064-128.pdf)

[Cached datasheet](datasheets/IS25LP032-064-128.pdf)

![amplifier](images/11-flash.png)

## U12 Monolithic Power Step-down Switching Regulator

[MPS Step-down Switching Regulator](https://www.monolithicpower.com/en/mp4559.html)

### Part number

    MP4559DN
    J3777318
    MPSJ21

### Datasheet

[Vendor datasheet](https://www.monolithicpower.com/pub/media/document/MP4559_r1.01.pdf)

[Cached datasheet](datasheets/MP4559_r1.01.pdf)

![amplifier](images/12-regulator.png)

## U13 JTAG / SWD Connector

![amplifier](images/13-jtag.png)

## U14 Panasonic Battery

[Panasonic Coin type lithium batteries (CR series)](https://industrial.panasonic.com/ww/products/batteries/primary-batteries/lithium-batteries/coin-type-lithium-batteries-cr-series/CR2032)

### Part number

    CR 2032
    3V

### Datasheet

[Vendor datasheet](https://industrial.panasonic.com/cdbs/www-data/pdf/AAA4000/AAA4000COL32.pdf)

[Cached datasheet](datasheets/AAA4000COL32.pdf)

![amplifier](images/14-battery.png)
